KJPush介绍
============
KJPush是一个Android推送框架，目标是让Android开发者一人无需与服务端开发者交流也能完成推送功能开发
#实现
不同于现有的第三方推送服务(极光、个推、百度等)以socket长连接的形式实现推送，KJPush采用轮询机制更适合轻量级应用快速完成推送功能开发。很多人认为长连接没有任何消耗，其实不然。如果轮询策略配置的好，消耗的电与数据流量绝不比维持一个socket连接使用的多。
#说明
####适用范围
采用轮询机制实现的推送，并不适合做聊天功能而更适合于开发APP通知功能。因为聊天需要高时效性，但轮询机制为了保证低功耗，需要牺牲数据的时效性；而由于降低了与服务器的对话频率，更加适合于对时效性要求不高的APP的通知推送功能。
####与长连接方式比较
采用长连接的方式优势在于数据相应及时，缺点在于不仅需要客户端与服务器端都采用相应的实现且配置与实现都比较复杂。采用轮询的优势在于实现便捷，服务器端无需额外的改动任何代码，客户端也只需要两句代码即可。不管是采用长连接或是轮询机制都面临着两个问题：费电与耗流量。目前KJPush在GPRS模式下采用4分钟请求一次服务器，WiFi模式下2分钟请求一次服务器。
####耗电与耗流量情况
电量消耗主要存在于两个地方：计时器需要一直工作，网络交互很耗电。计时器的问题目前KJPush采取的策略是使用系统AlarmManager心跳响应每2分钟一次，相对于自己额外实现定时器不管是用Sleep，TimerTask，都一定是会需要额外的系统消耗，也就直接造成手机的电量消耗，同时也加大了进程被系统回收的可能。对于网络交互的处理，默认设置为当手机屏幕熄灭便停止网络数据请求的功能，当屏幕熄灭20秒以后彻底停止推送进程，当手机屏幕亮起时再重新打开推送进程并发起网络数据请求。也就是说当屏幕熄灭以后KJPush是完全没有任何消耗。当然，开放了定制化接口，你可以自由定制手机熄灭与亮起后的操作以及AlarmManager心跳间隔。

#在项目中使用
同现有的第三方推送使用方法完全一致。
#一、你有两种选择实现方式：
1、在AndroidManifest的<application>标签内添加android:name="org.kymjs.push.core.KJPushManager";
2、如果你已经有自己的Application类，可以选择将自己的Application类继承自KJPushManager

#二、在你项目的AndroidManifest文件中添加如下声明：
```xml
	<service
            android:name="org.kymjs.push.core.PullService"
            android:persistent="true"
            android:process=":kymjs_pullservice" >
            <intent-filter android:priority="1000" >
                <action android:name="org.kymjs.push.service.pull_service" />
            </intent-filter>
        </service>

        <receiver
            android:name="org.kymjs.push.core.PushReceiver"
            android:process=":kymjs_pullservice" >
            <intent-filter>
                <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />
            </intent-filter>
        </receiver>
```
#三、当你想停止推送时调用
```java
getApplication().stopWork();
```